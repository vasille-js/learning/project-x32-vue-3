

export class Block {
    constructor (x, y) {
        this.x = x;
        this.y = y;
    }
}

export class BrickBlock extends Block {
    constructor (x, y) {
        super(x, y);
        this.type = 1;
    }
}

export class BetonBlock extends Block {
    constructor (x, y) {
        super(x, y);
        this.type = 2;
    }
}


export class BlockPack {
    constructor (x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.els = [];
        this.sub = [];
    }
}
